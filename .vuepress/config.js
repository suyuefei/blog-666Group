module.exports = {
  "title": "666-blog",
  "base": "/blog-666group/",
  "description": "第二届字节跳动青训营-寒假前端场-进阶班-666组",
  "dest": "public",
  "locales": { '/': { lang: 'zh-CN' } },
  "head": [
    [
      "link",
      {
        "rel": "icon",
        "href": "/favicon.ico"
      }
    ],
    [
      "meta",
      {
        "name": "viewport",
        "content": "width=device-width,initial-scale=1,user-scalable=no"
      }
    ]
  ],
  "theme": "reco",
  "subSidebar": 'auto',//在所有页面中启用自动生成子侧边栏，原 sidebar 仍然兼容
  "sidebar": "auto",//所有页面自动生成侧边栏
  "themeConfig": {
    "nav": [
      {
        "text": "主页",
        "link": "/",
        "icon": "reco-home"
      },
      {
        "text": "时间轴",
        "link": "/timeline/",
        "icon": "reco-date"
      },
      {
        "text": "留言板",
        "link": "/docs/message/messageBoard",//留言板功能
        "icon": "reco-suggestion",
      },
      {
        "text": "关于",
        "link": "/docs/about/author",//跳转团队简介页面
        "icon": "reco-message",
      }
    ],
    "sidebar": {
      "/blogs/suyuefei/": [
        "01",
        "02",
      ],
      "/blogs/fanzhixuan/": [
        "one",
        "two",
      ],
      "/blogs/liuyuheng/": [
        "one",
      ],
      "/blogs/wangyuhang/": [
        "one",
        "two",
      ],
      "/blogs/xuemingkun/": [
        "one",
        "two",
        "three"
      ],
      "/blogs/zenglizhi/": [
        "one",
      ]
    },
    "type": "blog",
    "blogConfig": {
      "category": {
        "location": 2,
        "text": "分类"
      },
      "tag": {
        "location": 3,
        "text": "标签"
      }
    },
    "friendLink": [
      {
        "title": "午后南杂",
        "desc": "Enjoy when you can, and endure when you must.",
        "email": "1156743527@qq.com",
        "link": "https://www.recoluan.com"
      },
      {
        "title": "vuepress-theme-reco",
        "desc": "A simple and beautiful vuepress Blog & Doc theme.",
        "avatar": "https://vuepress-theme-reco.recoluan.com/icon_vuepress_reco.png",
        "link": "https://vuepress-theme-reco.recoluan.com"
      }
    ],
    "logo": "/logo.png",
    "search": true,
    "searchMaxSuggestions": 10,
    "lastUpdated": "Last Updated",
    "author": "666",
    "authorAvatar": "/avatar.png",
    "record": "xxxx",
    "startYear": "2022"
  },
  "markdown": {
    "lineNumbers": true //代码显示行号
  },
  plugins: [
    [
      require("@vuepress-reco/vuepress-plugin-bgm-player"),
      {
        audios: [
          {
            name: '千与千寻',
            artist: '龙舟',
            url: './music/01.mp3', 
            cover: './music/千与千寻.png'
           }, {
            name: 'The Show',
            artist: '小石头和孩子们',
            url: './music/02.mp3',
            cover: './music/theShow.png'
          }, {
            name: 'Summer',
            artist: 'ひさいし じょう',
            url: './music/03.mp3',
            cover: './music/summer.png'
          }
        ],
        position: {
          left: '10px',
          bottom: '10px',
          'z-index': '999999'
        },
        autoplay: true,
        autoShrink: false,
        shrinkMode: 'float',
        floatPosition: 'left',
        floatStyle: {
          bottom: '200px',
          'z-index': '999999'
        }
      }
    ],
    [
      'dynamic-title',
      {
         showText: '(/≧▽≦/)耶！回来啦！',
         hideText: '(●—●)咦，不要走！',
         recoverTime: 1000,
      },
    ],
    ["vuepress-plugin-nuggets-style-copy", {
      copyText: "复制代码",
      tip: {
          content: "复制成功!"
      }
    }],
    [
      'vuepress-plugin-comment',
      {
        choosen: 'valine', 
        // options选项中的所有参数，会传给Valine的配置
        options: {
          el: '#valine-vuepress-comment',
          appId: 'i2lSQXO9PSg3UabhtzDsmy3z-gzGzoHsz',
          appKey: 'tMFwYwmDnRoTcKt56BG3CdIQ',
          width:'100px'
        }
      }
    ],
    [
      '@vuepress-reco/vuepress-plugin-kan-ban-niang',{
        theme: [
          'miku', 'whiteCat', 'haru1', 'haru2', 'haruto', 'koharu', 'izumi', 'shizuku', 'wanko', 'blackCat', 'z16'
        ],
        clean: false,
        messages: { 
          welcome: '欢迎来到我的博客', home: '心里的花，我想要带你回家。', theme: '好吧，希望你能喜欢我的其他小伙伴。', close: '你不喜欢我了吗？痴痴地望着你。' 
        },
        messageStyle: { right: '68px', bottom: '290px' },
        width: 250,
        height: 320
      }
    ],//看板娘
    [
      "vuepress-plugin-cursor-effects",
      {
        size: 2,                    //大小
        shape: 'circle',  // 形状
        zIndex: 999999999   // 优先级
      }
    ],//鼠标点击特效
    ["ribbon-animation", {
      size: 90,   // 默认数据
      opacity: 0.3,  //  透明度
      zIndex: -1,   //  层级
      opt: {
        // 色带HSL饱和度
        colorSaturation: "80%",
        // 色带HSL亮度量
        colorBrightness: "60%",
        // 带状颜色不透明度
        colorAlpha: 0.65,
        // 在HSL颜色空间中循环显示颜色的速度有多快
        colorCycleSpeed: 6,
        // 从哪一侧开始Y轴 (top|min, middle|center, bottom|max, random)
        verticalPosition: "center",
        // 到达屏幕另一侧的速度有多快
        horizontalSpeed: 200,
        // 在任何给定时间，屏幕上会保留多少条带
        ribbonCount: 2,
        // 添加笔划以及色带填充颜色
        strokeSize: 0,
        // 通过页面滚动上的因子垂直移动色带
        parallaxAmount: -0.5,
        // 随着时间的推移，为每个功能区添加动画效果
        animateSections: true
      },
      ribbonShow: false, //  点击彩带  true显示  false为不显示
      ribbonAnimationShow: true  // 滑动彩带
    }
  ],
  ]
}